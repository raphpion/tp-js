import { backgrounds, maps, music, sfx, sprites } from './assets.js'
import { getScene } from './screen.js'
import { menu_intro } from './menus/intro.js'

async function loadAssets() {
  // Fonction qui permet de charger toutes les ressources du jeu avant de le lancer
  await loadBackgrounds() // chargement des backgrounds
  await loadSprites() // ensuite, chargement des sprites
  await loadMaps() // ensuite, chargement des couches des maps
  await loadMusic() // ensuite, chargement des musiques
  await loadSFX() // ensuite, chargement des effets sonores

  // lorsque terminé, on passe à l'écran intro
  console.log(`All assets loaded succesfully! Entering game...`)
  getScene(menu_intro)
}

async function loadBackgrounds() {
  // on crée une liste de promesses à exécuter pour charger l'image de chaque background
  const promises = Object.keys(backgrounds).map(bg => {
    return new Promise((resolve, reject) => {
      // on charge l'image dans la balise HTML et on résout la promesse lorsque l'image a 'load'
      backgrounds[bg].src = `../assets/backgrounds/${bg}.png`
      backgrounds[bg].addEventListener('load', resolve)
    })
  })

  return Promise.all(promises)
}

async function loadSprites() {
  // on crée une liste de promesses à exécuter pour charger l'image de chaque sprite
  const promises = Object.keys(sprites).map(sprite => {
    return new Promise((resolve, reject) => {
      // on charge l'image dans la balise HTML et on résout la promesse lorsque l'image a 'load'
      sprites[sprite].src = `../assets/sprites/${sprite}.png`
      sprites[sprite].addEventListener('load', resolve)
    })
  })

  return Promise.all(promises)
}

async function loadMaps() {
  // on crée une liste de promesses à exécuter pour charger les images de tous les layers
  const promises = Object.keys(maps).map(layer => {
    return new Promise((resolve, reject) => {
      // on charge l'image dans la balise HTML et on résout la promesse lorsque l'image a 'load'
      maps[layer].src = `../assets/maps/${layer}.png`
      maps[layer].addEventListener('load', resolve)
    })
  })

  return Promise.all(promises)
}

async function loadMusic() {
  // on crée une liste de promesses à exécuter pour charger toutes les chansons
  const promises = Object.keys(music).map(song => {
    return new Promise((resolve, reject) => {
      // on charge le fichier dans la balise Audio et on résout la promesse lorsque le fichier peut être lu en entier
      music[song].src = `../assets/music/${song}.mp3`
      music[song].addEventListener('canplaythrough', resolve)
    })
  })

  return Promise.all(promises)
}

async function loadSFX() {
  // on crée une liste de promesses à exécuter pour charger tous les SFX
  const promises = Object.keys(sfx).map(sound => {
    return new Promise((resolve, reject) => {
      // on charge le fichier dans la balise Audio et on résout la promesse lorsque le fichier peut être lu en entier
      sfx[sound].src = `../assets/sfx/${sound}.wav`
      sfx[sound].addEventListener('canplaythrough', resolve)
    })
  })

  return Promise.all(promises)
}

export { loadAssets }
