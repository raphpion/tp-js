import { loadAssets } from '../loader.js'
import { screen, ctx, GAME_WIDTH, GAME_HEIGHT, clearScreen } from '../screen.js'

// Écran de chargement
const menu_loading = {
  background: 'black',
  text: '',
  clear: () => {},
  draw: () => {
    // fonction d'affichage de l'écran de chargement à l'écran
    clearScreen()

    // texte
    ctx.font = '20pt VT323'
    ctx.fillStyle = 'white'
    ctx.textAlign = 'center'
    ctx.textBaseline = 'bottom'
    ctx.fillText(menu_loading.text, GAME_WIDTH / 2, GAME_HEIGHT / 2)
  },
  init: () => {
    // fonction d'initialisation de l'écran de chargement
    screen.style.background = menu_loading.background
    menu_loading.text = 'Chargement en cours...'

    // on démarre le loading
    loadAssets()

    // on retourne l'intervalle d'affichage
    return setInterval(menu_loading.draw(), 1000 / 60)
  },
}

export { menu_loading }
