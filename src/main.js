//* NOOSA'S WORLD
/*
  Noosa s'est échappée de la maison et part à l'aventure! Elle doit ramasser le plus
  de croquettes tout en évitant les bourdons.

  Si vous rencontrez des problèmes avec la prévisualisation dans Brackets ou avec le
  Live Server de VS Code, une version jouable du même code est disponible en ligne à
  l'adresse suivante : https://noosas-world.netlify.app/
*/
// Release v1.0 ─ 1er décembre 2020
// © Copyright Raphaël Pion 2020

import { initialSettings } from './settings.js'
import { getScene } from './screen.js'
import { menu_loading } from './menus/loading.js'

console.log(`Noosa's World running version 1.0`)

// Vérification de l'intégrité des options utilisateurs
initialSettings()

// Chargement des ressources du jeu
getScene(menu_loading)
