# Noosa's World
Noosa's World est un **Platformer 2D** développé par Raphaël Pion dans le cadre du cours *Animation et Interactivité en jeu*, à la Technique d'Intégration Multimédia du Collège Maisonneuve.

Une version jouable entièrement en ligne est disponible à l'adresse suivante : https://noosas-world.netlify.app/

## Développement
Le jeu est entièrement développé en Vanilla JavaScript ES6 et utilise les nouvelles fonctionnalités de HTML5 et CSS3, dont les *canvas*.

Quelques sprites proviennent du jeu *Super Mario World* par Nintendo, une des inspirations principales pour la conception du jeu mais la majorité des ressources qui ont permis la réalisation ont été produites par Raphaël Pion.

Merci à [Gabriel Cholette-Rioux](https://gcholette.com/) pour son aide précieuse !

## Projets futurs
Pour l'année 2021, l'intégration de support pour appareil mobile, tablette et manette sont prévus. Un mode histoire a aussi commencé à être conçu, son développement suivra après le développement de la mise à jour de compatibilité.